from flask import render_template, current_app as app
import psutil
import platform
import datetime

# Conditionally import cpuinfo
supported_architectures = ['x86_64', 'AMD64', 'i386', 'armv7l', 'aarch64', 'ppc64le', 's390x']

if platform.machine() in supported_architectures:
    import cpuinfo
else:
    cpuinfo = None

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/info")
def info():
    osinfo = {}
    osinfo["plat"] = platform
    if cpuinfo:
        osinfo["cpu"] = cpuinfo.get_cpu_info()
    else:
        osinfo["cpu"] = "CPU information not available on this architecture."
    osinfo["mem"] = psutil.virtual_memory()
    osinfo["net"] = psutil.net_if_addrs()
    osinfo["boottime"] = datetime.datetime.fromtimestamp(psutil.boot_time()).strftime(
        "%Y-%m-%d %H:%M:%S"
    )

    return render_template("info.html", info=osinfo)

@app.route("/monitor")
def monitor():
    return render_template("monitor.html")
